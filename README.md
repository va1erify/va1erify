# Участник сообщества [DeusOps](https://deusops.com/)

## Links

- [TicTacToe](https://gitlab.com/va1erify-java/Projects/TicTacToe): TicTacToe с несколькими уровнями сложностями (Java, Swing);
- [Calculator](https://gitlab.com/va1erify-java/Projects/Calculator): Калькулятор (Java, JavaFX);
- [Chat](https://gitlab.com/va1erify-java/Projects/Chat): Многопользовательский чат (Java, JavaFX);
- [Network Storage](https://gitlab.com/va1erify-java/Projects/NetworkStorage): Многопользовательское сетевое хранилище (Java, JavaFX, Netty);
- [Store](https://gitlab.com/va1erify-java/Projects/Store): Интернет магазин. Монолитная архитектура, впоследствии распиленная на микросервисы (Java, SQLite, Spring Boot, Spring Web, Spring Security, Spring Cloud, Spring Data);
- [Javarush](https://javarush.com/users/2899159): 40 уровень (max) на платформе JavaRush (Арканоид, Змейка, Ипподром, MVC и т.д);
- [Algorithms](https://gitlab.com/va1erify-algorithms): Алгоритмы поиска, сортировки. Структуры данных. Задачи с Leetcode и Codewars.
- [Ansible roles](https://gitlab.com/va1erify-ansible-roles): Ansible роли;
- [Terraform modules](https://gitlab.com/va1erify-terraform/yandex-modules/modules): Terraform модули для Yandex-Cloud;
- [Helm charts](https://gitlab.com/va1erify-helm-charts): Helm чарты;
- [Docker images](https://gitlab.com/va1erify-docker-images): Docker образы;
- [Gitlab-ci](https://gitlab.com/va1erify-gitlab-ci): Переиспользуемые gitlab-ci пайплайны;
- [26-01-24](https://gitlab.com/va1erify-deusops/task-26-01-2024): Поэтапная миграция приложения из Docker в Kubernetes (Systemd, Ansible, Bash, Docker-compose, Haproxy, Yandex Cloud, Terraform, Kubernetes, Helm, Nginx-ingress controller, Cert-manager, LetsEncrypt);
- [22-03-24](https://gitlab.com/va1erify-deusops/task-22-03-24): Деплой приложения на dev и prod без использования Kubernetes (Docker, Docker-compose, Docker Hub, Terraform, Yandex Cloud, Ansible, Gitlab, Python, MySQL, Certbot, Nginx, Traefik, Portainer, Bash, LetsEncrypt);
- [18-05-24](https://gitlab.com/va1erify-deusops/task-18-05-24): Автоматизация развертывания (dev, stage, prod) в Kubernetes через Helm, бизнес и инфраструктурный мониторинг (Yandex Cloud, Terraform, Kubernetes, Helm, Docker, Docker Hub, Python, Gitlab CI, Prometheus, Grafana, Ingress-Nginx controller, Cert-manager, LetsEncrypt, Bash);
- [10-08-24](https://gitlab.com/va1erify-deusops/task-10-08-24): Реализация MLOps для ML-специалистов (NVIDIA, Apache AirFlow, DAG, Yandex Cloud, Terraform, Kubernetes, Helm, Postgres, Ansible, Ansible Molecule, Gitlab CI);
- [Zakroma](https://gitlab.com/zakroma): Zakroma infrastructure flow